package ljmu.vets;
import java.io.Serializable;

import java.util.List;

public interface Saveable {
	
	
	
	public void serialize(List<Surgery> Surgeries);
	public List<Surgery> deserialize();
	
	}

