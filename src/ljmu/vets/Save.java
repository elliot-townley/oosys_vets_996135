package ljmu.vets;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.List;

public class Save implements Serializable{


	public void saveAs(FileType fileType, String path) {
		switch (fileType) {
			case DOCX : {
				saveAsDocX(path);
				break;
			}
			case XPS : {
				saveAsXPS(path);
				break;
			}
			case PDF : {
				saveAsPDF(path);
				break;
			}
		}
	}
	
	// ToDo : Private !
	private void saveAsDocX(String path) {
		
	}

	// ToDo : Private !
	private void saveAsXPS(String path) {
		
	}

	// ToDo : Private !
	private void saveAsPDF(String path) {
		
	}

	// ToDo : get / set Methods ?
}
