package ljmu.vets;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.List;

public class Invoice implements Serializable {
	private Integer no;
	private LocalDateTime when;
	private Double amount;
	private List<Booking> bookings;
	private List<PublicPayable> publicPayables;
	private List<SurgeryPayable>surgeryPayables;
	

	public Invoice(Integer no, LocalDateTime when, Double amount, List<Booking> bookings,  List<SurgeryPayable> surgeryPayables, List<PublicPayable> publicPayables) {
		this.no = no;
		this.when = when;
		// this.amount = amount;
		this.amount = calculateAmount(bookings);

		this.bookings = bookings;
		this.publicPayables = publicPayables;
		this.surgeryPayables = surgeryPayables;
	}

	private Double calculateAmount(List<Booking> bookings) {
		Double tt = 0.0;

		for (Booking o : bookings) {
			
			tt += o.getDuration() * o.getPet().getRate();
			

		}

		return Double.parseDouble(new DecimalFormat("#.##").format(tt));
	}



	// ToDo : get / set Methods ?
}
